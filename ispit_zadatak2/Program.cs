﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ispit_zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Unesite dimenziju niza:");
            int.TryParse(Console.ReadLine(), out int n);

            int[] niz = new int[n];

            for(int i = 0; i < niz.Length; i++)
            {
                Console.Write($"[{i}]: ");
                int.TryParse(Console.ReadLine(), out niz[i]);
            }
            Console.WriteLine();
            int sParnih = 0;
            int pNeparnih = 1;
            for(int i = 0; i < niz.Length; i++)
            {
                if (i % 2 == 0)
                {
                    sParnih += niz[i];
                }
                else
                {
                    pNeparnih *= niz[i];
                }
            }
            Console.WriteLine($"Proizvod elemenata sa neparnim indexom je {pNeparnih}");
            Console.WriteLine($"Suma elemenata sa parnim indexom je {sParnih}");
            Console.WriteLine();
            if (pNeparnih > sParnih)
            {
                Console.WriteLine($"Proizvod vrednosti elemenata sa neparnim indexom je veca!{pNeparnih} > {sParnih}");
            }
            else
            {
                Console.WriteLine($"Suma vrednosti elemenata sa parnim indexom je veca!{pNeparnih} < {sParnih}");
            }
        }
    }
}
